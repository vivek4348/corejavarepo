package common;

public class SubClass extends SuperClass {

	public static void staticMethod() {
		System.out.println("In SubClass staticMethod() ");
	}
	
	public void instanceMethod() {
		System.out.println("In SubClass instanceMethod() ");
	}
}
