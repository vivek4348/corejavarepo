package common;

public class SuperClass {

	public static void staticMethod() {
		System.out.println("In SuperClass staticMethod() ");
	}
	
	public void instanceMethod() {
		System.out.println("In SuperClass instanceMethod() ");
	}
}
