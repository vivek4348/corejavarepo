package exostar;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.SAXException;

public class ConverterUtils {
	
	private static final Logger log = Logger.getLogger(ConverterUtils.class);
	
	public static byte[] generateCSVFromExcel(byte[] inputByteArray, String fileType)
			throws FileNotFoundException, IOException, OpenXML4JException, ParserConfigurationException, SAXException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(baos);
		if (fileType.endsWith("xls")) {
			XLS2CSV xls2csv = new XLS2CSV(inputByteArray, printStream, -1);
			xls2csv.process();
		} else if (fileType.endsWith("xlsx")) {
			OPCPackage p = OPCPackage.open(new ByteArrayInputStream(inputByteArray));
			XLSX2CSV xlsx2csv = new XLSX2CSV(p, printStream, -1);
			xlsx2csv.process();
			p.close();
		} else {
			log.error("Unsupported File format (only CSV, XLS, XLSX are Allowed)"
					+ " to upload Users in generateCSVFromExcel ");
			throw new IOException("Unsupported File format (only CSV, XLS, XLSX are Allowed)"
					+ " to upload Users in generateCSVFromExcel ");
		}		
		byte[] outputByteArray = baos.toByteArray();
		/*String string = new String(outputByteArray,  StandardCharsets.UTF_8);
		log(string);*/
		return outputByteArray;
	}
	
	/**
	 * @throws IOException 
	 * 
	 */
	public static void csvToXLSX(String csvFileAddress, String xlsxFileAddress) throws IOException {

		XSSFWorkbook workBook = null;
		XSSFSheet sheet = null;
		FileOutputStream fileOutputStream = null;
		BufferedReader br = null;
		CellStyle style  = null;
		DataFormat format = null;
		try {
			workBook = new XSSFWorkbook();
			sheet = workBook.createSheet("sheet1");
			format = workBook.createDataFormat();
			String currentLine = null;
			int RowNum = 0;
			br = new BufferedReader(new FileReader(csvFileAddress));
			while ((currentLine = br.readLine()) != null) {
				String str[] = currentLine.split(",");
				RowNum++;
				XSSFRow currentRow = sheet.createRow(RowNum);
				for (int i = 0; i < str.length; i++) {
					XSSFCell cell = currentRow.createCell(i);
					if(StringUtils.isNumeric(str[i])) {
						style = workBook.createCellStyle();
						sheet.autoSizeColumn(i);
						style.setDataFormat(format.getFormat("0"));
						cell.setCellStyle(style);
						cell.setCellValue(Double.parseDouble(str[i]));
					} else {
						cell.setCellValue(str[i]);
					}
					
				}
			}
			fileOutputStream = new FileOutputStream(xlsxFileAddress);
			workBook.write(fileOutputStream);
		} catch (Exception ex) {
			log.error("Exception Occured when converting a CSV file to xlsx File in csvToXLSX " + ex.getMessage());
			throw ex;
		} finally {
			br.close();
			workBook.close();
			fileOutputStream.close();			
		}
	}
	/**
	 * @throws IOException 
	 * 
	 */
	public static void csvToXLS(String csvFileAddress, String xlsFileAddress) throws IOException {

		HSSFWorkbook workBook = null;
		HSSFSheet sheet = null;
		FileOutputStream fileOutputStream = null;
		BufferedReader br = null;
		CellStyle style  = null;
		DataFormat format = null;
		try {
			workBook = new HSSFWorkbook();
			sheet = workBook.createSheet("sheet1");
			format = workBook.createDataFormat();
			String currentLine = null;
			int RowNum = 0;
			br = new BufferedReader(new FileReader(csvFileAddress));
			while ((currentLine = br.readLine()) != null) {
				String str[] = currentLine.split(",");
				RowNum++;
				HSSFRow currentRow = sheet.createRow(RowNum);
				for (int i = 0; i < str.length; i++) {
					HSSFCell cell = currentRow.createCell(i);
					if(StringUtils.isNumeric(str[i])) {
						style = workBook.createCellStyle();
						sheet.autoSizeColumn(i);
						style.setDataFormat(format.getFormat("0"));
						cell.setCellStyle(style);
						cell.setCellValue(Double.parseDouble(str[i]));
					} else {
						cell.setCellValue(str[i]);
					}
					
				}
			}
			fileOutputStream = new FileOutputStream(xlsFileAddress);
			workBook.write(fileOutputStream);
		} catch (Exception ex) {
			log.error("Exception Occured when converting a CSV file to xls File in csvToXLS " + ex.getMessage());
			throw ex;
		} finally {
			br.close();
			workBook.close();
			fileOutputStream.close();		
		}
	}
	
	
	
}
