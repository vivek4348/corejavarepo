package main;

import java.io.IOException;

public class ExceptionsClassification {

	public static void main(String[] args) {
		System.out.println("GIT TEST");
		try {
			getCheckedException(); // Some External Entity Caused this.
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		geUnCheckedException(); // Programmer Made Mistakes.
		testRunTimeException(); // Conclusion: RuntimeException is Unchecked Exception. You dont have to have a ExceptionHandler for RuntimeException.
		
		
		try {
			testClassNotFoundException();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		testNoClassDefFoundError(); // Error derived from LinkageError class,
									// where a class has a dependency on some other class and that class has incompatibly changed after the compilation.
		/*Both NoClassDefFoundError and ClassNotFoundException are related to unavailability of a class at run-time.*/
	}

	private static void testClassNotFoundException() throws ClassNotFoundException {
		// TODO Auto-generated method stub
		
	}

	private static void testNoClassDefFoundError()  throws NoClassDefFoundError {
		// TODO Auto-generated method stub
		
	}

	private static void testRunTimeException() throws RuntimeException {
				
	}

	private static void geUnCheckedException() throws ArrayIndexOutOfBoundsException {
				
	}

	private static void getCheckedException() throws IOException{
				
	}
		
}
