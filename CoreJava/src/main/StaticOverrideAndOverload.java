package main;
import common.SubClass;
import common.SuperClass;

public class StaticOverrideAndOverload {

	public static void main(String[] args) {
		SuperClass superClass = new SuperClass();
		SuperClass subClass = new SubClass();
		
		superClass.staticMethod();
		superClass.instanceMethod();
		
		subClass.staticMethod();
		subClass.instanceMethod();
		
	}

	
		
}
/*Output
------
In SuperClass staticMethod() 
In SuperClass instanceMethod() 
In SuperClass staticMethod() ----------> This shows static methods cannot be overridden. Superclass static method is executed.
In SubClass instanceMethod() */