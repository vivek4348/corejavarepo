package main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMain {
	
	public static void main(String[] args) {
	/*	System.out.println(getEnumValue("","airlvvine"));
		System.out.println(getEnumValue("","MRO"));
		System.out.println(getEnumValue("","OTHER"));
		
		System.out.println(getEnumValue("","Airline"));
		System.out.println(getEnumValue("","Maintenance, REPAIR and Overhaul"));
		System.out.println(getEnumValue("","Other"))*/;
		
		System.out.println(System.getProperty("key1"));
		System.out.println(System.getProperty("javax.net.ssl.keyStore"));
		System.out.println(System.getProperty("javax.net.ssl.keyStorePassword"));
		System.out.println(System.getProperty("javax.net.ssl.keyStoreType"));
	}
	
	public static String getEnumValue(String type, String refVal) {		
		Map<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("airline", "Airline");
		hashMap.put("mro", "Maintenance, Repair and Overhaul");
		hashMap.put("other", "Other");
		for (String key : hashMap.keySet()) {
			if (refVal.equalsIgnoreCase(key) || hashMap.get(key).equalsIgnoreCase(refVal)) {
				return key;
			}
		}	
		return null;
	} 


public static String convertMessages(List<String> messages) {
	StringBuilder stringBuilder = new StringBuilder(); 
	for (String errorMessage : messages) {
		stringBuilder.append("\n").append(errorMessage);
	}
	return stringBuilder.toString().replaceFirst("\n", "");
}
}