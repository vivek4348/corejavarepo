package util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

public class VivekUtil {
	
		// '<T>' is a Generic Type Parameter; Collection<T> is return type.
		public static <T> Collection<T> safe(Collection<T> collection) {
			 return collection == null ? Collections.emptyList() : collection;
		}
		
		public static <T> void printCollection(Collection<T> collection) {
			for (T t : collection) {
				System.out.println(t);
			}		 
		}
		
		public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
		    return map.entrySet()
		              .stream()
		              .filter(entry -> Objects.equals(entry.getValue(), value))
		              .map(Map.Entry::getKey)
		              .collect(Collectors.toSet());
		}
		
		public static <T> Set<T> getDuplicates(Collection<T> list) {
		       Set<T> uniques = new HashSet<T>();
		       return list.stream().filter(e -> !uniques.add(e)).collect(Collectors.toSet());
		   }
		
		public static String getEnumValue(Map<String, String> vals, String refVal) {		
			for (String key : vals.keySet()) {
				if (refVal.equalsIgnoreCase(key) || vals.get(key).equalsIgnoreCase(refVal)) {
					return key;
				}
			}
			return null;
		}
		
		public static List<String> convertSemicolonDelimitedStringToList(String adminAppsString) {
			List<String> stringList = new LinkedList<>();
			if(StringUtils.isNotBlank(adminAppsString)) {
				String [] stringArray = adminAppsString.split("\\s*;\\s*");
				for (String string : stringArray) {
					stringList.add(string.trim());
				}
			}
			return stringList;
		}
		
		public static String convertMessages(List<ErrorMessage> messages) {
			StringBuilder stringBuilder = new StringBuilder(); 
			for (ErrorMessage errorMessage : safe(messages)) {
				stringBuilder.append(";").append(errorMessage.getName());
			}
			return stringBuilder.toString().replaceFirst(";", "");
		}

		public static String safe(String str) {
			if(StringUtils.isBlank(str)) {
				return "";
			}
			return str;
		}
		
		public static boolean isValidName(String name, String targetProperty) {
			return name.chars().allMatch(Character::isLetter);			
		}
				

}
